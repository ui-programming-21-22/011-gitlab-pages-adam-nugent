const SCALE = 2;
const WIDTH = 16;
const HEIGHT = 18;
const SCALED_WIDTH = SCALE * WIDTH;
const SCALED_HEIGHT = SCALE * HEIGHT;
const CYCLE_LOOP = [0, 1, 0, 2];
const FACING_DOWN = 0;
const FACING_UP = 1;
const FACING_LEFT = 2;
const FACING_RIGHT = 3;
const FRAME_LIMIT = 12;
const foodWidth = 32;
const foodHeight = 32;

//Score / Speed / Health
let score = 0;
let speed = 1;
let health = 300
let storedHealth = localStorage.getItem("health");
if(storedHealth){
  health = storedHealth;
  console.log("Health Retrieved");
}
else{
  health = 300;
  localStorage.setItem("health", health);
  console.log("Health Initiated in memory");
}
let storedScore = localStorage.getItem("score");
if(storedScore){
  score = storedScore;
  console.log("Score Retrieved");
}
else{
  localStorage.setItem("score", score);
  console.log("Score Initiated in memory");
}

//food variables
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomFoodX = Math.abs(Math.floor(Math.random() * 7));
let randomFoodY = Math.abs(Math.floor(Math.random() * 4));

//Score Variables
let scoreCount = 0;
if (score){
    scoreCount = score;
}


//CLOCK
setInterval(displayTime, 1000);
function displayTime() {

    const timeNow = new Date();

    let hoursOfDay = timeNow.getHours();
    let minutes = timeNow.getMinutes();
    let seconds = timeNow.getSeconds();
    let weekDay = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    let today = weekDay[timeNow.getDay()];
    let months = timeNow.toLocaleString("default", {
        month: "long"
    });
    let year = timeNow.getFullYear();
    let period = "AM";

    if (hoursOfDay > 12) {
        hoursOfDay-= 12;
        period = "PM";
    }

    if (hoursOfDay === 0) {
        hoursOfDay = 12;
        period = "AM";
    }

    hoursOfDay = hoursOfDay < 10 ? "0" + hoursOfDay : hoursOfDay;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    let time = hoursOfDay + ":" + minutes + ":" + seconds + period;

    document.getElementById('Clock').innerHTML = time + " " + today + " " + months + " " + year;
  }

  function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}
  // Images
  let character = new Image();
  character.src = "assets/image/Green-Cap.png";

  let foodSprite = new Image();
  foodSprite.src = "assets/image/fruit.png";


// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
  this.spritesheet = spritesheet;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.movementDirection = "None";
}

// Default Player
let player = new GameObject(character, 10, 10, 200, 200);

//Item Pick-up
let foods = new GameObject(foodSprite, randomX, randomY, 100, 100);



//HealthBar, increasing health value will not change the size.
function drawHealthbar() {
  var width = 100;
  var height = 20;
  var maxHealth = 100;
  
  // Draw the background
  context.fillStyle = "green";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);
  
  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(health / maxHealth, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);
}
  
let canvas = document.querySelector('canvas');
let context = canvas.getContext('2d');
let keyPresses = {};
let currentDirection = FACING_DOWN;
let currentLoopIndex = 0;
let frameCount = 0;
let positionX = 0;
let positionY = 0;
let img = new Image();

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
  this.spritesheet = spritesheet;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.movementDirection = "None";
}

function update(){

  let hasMoved = false;
  //console.log(player);
    // Check Input
    if (gamerInput.action === "Up") {
      if (player.y < 0){
          console.log("player at top edge");
          health -= 1;
          localStorage.setItem("health", health)
      }
      else{
          player.y -= speed; // Move Player Up
      }
      currentDirection = 1;
  } else if (gamerInput.action === "Down") {
      if (player.y + SCALED_HEIGHT > canvas.height){
          console.log("player at bottom edge");
          health -= 1;
          localStorage.setItem("health", health)
      }
      else{
          player.y += speed; // Move Player Down
      }
      currentDirection = 0;
  } else if (gamerInput.action === "Left") {
      if (player.x < 0){
          console.log("player at left edge");
          health -= 1;
          localStorage.setItem("health", health)
      }
      else{
          player.x -= speed; // Move Player Left
      }
      currentDirection = 2;
  } else if (gamerInput.action === "Right") {
      if (player.x + SCALED_WIDTH > canvas.width){
        console.log("player at right edge");
        health -= 1;
        localStorage.setItem("health", health)
      }
      else{
          player.x += speed; // Move Player Right
      }
      currentDirection = 3;
  } else if (gamerInput.action === "None") {
  }
}

function drawFrame(img, frameX, frameY, canvasX, canvasY) {
  context.drawImage(img,
                frameX * WIDTH, frameY * HEIGHT, WIDTH, HEIGHT,
                canvasX, canvasY, SCALED_WIDTH, SCALED_HEIGHT);
}

//starts the game
function startGame(){
  if (gamerInput.action === "Start")
  {
      start = 1;
      startTXT.innerHTML = " ";
  }
}

//mobile controls
// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
  this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
  // Take Input from the Player
  // console.log("Input");
  //console.log("Event type: " + event.type);
  //console.log("Keycode: " + event.keyCode);

  if (event.type === "keydown") {
      switch (event.keyCode) {
          case 37: // Left Arrow
              gamerInput = new GamerInput("Left");
              break; //Left key
          case 38: // Up Arrow
              gamerInput = new GamerInput("Up");
              break; //Up key
          case 39: // Right Arrow
              gamerInput = new GamerInput("Right");
              break; //Right key
          case 40: // Down Arrow
              gamerInput = new GamerInput("Down");
              break; //Down key
          case 87: // w key
              gamerInput = new GamerInput("Up");
              break; //Up key
          case 65: // a
              gamerInput = new GamerInput("Left");
              break; //Left key
          case 83: // s
              gamerInput = new GamerInput("Down");
              break; //Down key
          case 68: // d
              gamerInput = new GamerInput("Right");
              break; //Right key
          default:
              gamerInput = new GamerInput("None"); //No Input
      }
  } else {
      gamerInput = new GamerInput("None");
      speed = 2;
  }
}
function pressUp(){
  gamerInput = new GamerInput("Up");
};
function pressDown(){
  gamerInput = new GamerInput("Down");
};
function pressLeft(){
  gamerInput = new GamerInput("Left");
};
function pressRight(){
  gamerInput = new GamerInput("Right");
};
function noMoving(){
  gamerInput = new GamerInput("None");
}

/*
var dynamic = nipplejs.create({
  color: 'purple',
});


dynamic.on('added', function (evt, nipple) {
  //nipple.on('start move end dir plain', function (evt) {
  nipple.on('dir:up', function (evt, data) {
     //console.log("direction up");
     gamerInput = new GamerInput("Up");
  });
  nipple.on('dir:down', function (evt, data) {
      //console.log("direction down");
      gamerInput = new GamerInput("Down");
   });
   nipple.on('dir:left', function (evt, data) {
      //console.log("direction left");
      gamerInput = new GamerInput("Left");
   });
   nipple.on('dir:right', function (evt, data) {
      //console.log("direction right");
      gamerInput = new GamerInput("Right");
   });
   nipple.on('end', function (evt, data) {
      //console.log("movement stopped");
      gamerInput = new GamerInput("None");
   });
});
*/

window.addEventListener('keydown', input);


window.addEventListener('keyup', input);



function animate(){
  if (gamerInput.action != "None") {
    frameCount++;
    if (frameCount >= FRAME_LIMIT) {
      frameCount = 0;
      currentLoopIndex++;
      if (currentLoopIndex >= CYCLE_LOOP.length) {
        currentLoopIndex = 0;
      }
    }
  }
}

foodPosition = new randoPos(499, 499, 50);

function newFood(){
  randomFoodX = Math.abs(Math.floor(Math.random() * 7));
  randomFoodXSelect = randomFoodX * 64;
  randomFoodY = Math.abs(Math.floor(Math.random() * 4));
  randomFoodYSelect = randomFoodY * 64;
  foodPosition = new randoPos(499, 499, 50);
}


function manageFood(){
  // place the piece of food
  context.drawImage(foodSprite, randomFoodX*64, randomFoodY*64, 64, 64, foodPosition.x, foodPosition.y, foodWidth, foodHeight);
  // check for collision (eating)

  if (foodPosition.x < player.x + SCALED_WIDTH && //collision from left to right
  foodPosition.x + foodWidth > player.x && // collision from right to left
  foodPosition.y < player.y + SCALED_HEIGHT && // collision from top to bottom
  foodPosition.y + foodHeight > player.y // collision from bottom to top
  ){
    console.log("collision!");
        scoreCount ++;
        localStorage.setItem("score", scoreCount);
        newFood();
  }
  // place new food
}

function writeScore(){
  let scoreString = "score: " + scoreCount;
  context.font = '22px sans-serif';
  context.fillText(scoreString, 200, 200)
}



function draw(){
  animate();
  manageFood();
  writeScore();
  drawFrame(player.spritesheet, CYCLE_LOOP[currentLoopIndex], currentDirection, player.x, player.y);
}


function gameLoop() {
  update();
  drawHealthbar();
  draw();
  displayTime();
  window.requestAnimationFrame(gameLoop);
}

window.requestAnimationFrame(gameLoop);